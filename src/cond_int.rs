mod imp {
    use std::marker::PhantomData;

    pub trait Nat {}
    pub struct Z;
    pub struct S<N: Nat>(PhantomData<N>);
    impl Nat for Z {}
    impl<N: Nat> Nat for S<N> {}

    pub struct CondInt<T, N: Nat>(PhantomData<T>, PhantomData<N>);

    pub trait Reduce {
        type Reduced: Reduce;
        type Internal;
    }

    trait AddOne<const N: usize> {
        const SN: usize = N + 1;
    }

    impl<const N: usize> AddOne<N> for () {}

    impl<T1, T2, N: Nat> Reduce for CondInt<(T1, T2), S<N>>
    where
        CondInt<T2, N>: Reduce,
    {
        type Reduced = <CondInt<T2, N> as Reduce>::Reduced;
        type Internal = ();
    }

    impl<T1, T2> Reduce for CondInt<(T1, T2), Z> {
        type Reduced = Self;
        type Internal = T1;
    }
}

pub type N0 = imp::Z;
pub type N1 = imp::S<N0>;
pub type N2 = imp::S<N1>;
pub type N3 = imp::S<N2>;
pub type N4 = imp::S<N3>;
pub type N5 = imp::S<N4>;

pub type CondInt<T, N> = <<imp::CondInt<T, N> as imp::Reduce>::Reduced as imp::Reduce>::Internal;

pub enum Never {}

#[macro_export]
macro_rules! form_tuple {
    ($a:ty) => {
        ($a, $crate::cond_int::Never)
    };
    ($a:ty, $($b:tt)*) => {
        ($a, form_tuple!($($b)*))
    }
}

#[macro_export]
macro_rules! cond_int {
    ($n:ty, $($t:tt)*) => {
        $crate::cond_int::CondInt<form_tuple!($($t)*), $n>
    }
}
