#![feature(specialization, generic_const_exprs)]
#![deny(clippy::all)]
#![allow(dead_code, incomplete_features)]

mod align;
mod cond;
mod cond_int;
mod const_index;
mod copyable;
mod iterator;

//type AlignType = u32; // Alignment: 4, Size: 12

type AlignType = u64; // Alignment: 8, Size: 16

use iterator::{ConstSplit, UnzipArray, ZipArray};
fn main() {
    println!(
        "{:?}",
        [[0, 2, 3], [4, 1, 2], [3, 4, 9], [2, 8, 3]]
            .zip()
            .unzip_array()
            .into_iter()
            .map(|a| a.collect::<Vec<_>>())
            .collect::<Vec<_>>()
    );
    let s: [_; 3] = (0..10).split();
    println!("{:?}", s.zip().collect::<Vec<_>>());
}
