pub struct Align<A, T> {
    alignment: [A; 0],
    inner: T,
}

impl<A, T> Align<A, T> {
    pub fn new(inner: T) -> Self {
        Align {
            alignment: [],
            inner,
        }
    }
}

impl<A, T> From<T> for Align<A, T> {
    fn from(inner: T) -> Self {
        Align {
            alignment: [],
            inner,
        }
    }
}

impl<A, T> std::ops::Deref for Align<A, T> {
    type Target = T;
    fn deref(&self) -> &T {
        &self.inner
    }
}
