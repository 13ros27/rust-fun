use std::ops::Index;

trait IsValidIndex<Target> {
    const RESULT: ();
}

pub struct ConstIndex<const I: usize>;

impl<T, const I: usize, const N: usize> IsValidIndex<[T; N]> for ConstIndex<I> {
    const RESULT: () = assert!(N > I, "Index is out of bounds!");
}

impl<T, const I: usize, const N: usize> Index<ConstIndex<I>> for [T; N] {
    type Output = T;
    fn index(&self, _: ConstIndex<I>) -> &T {
        #[allow(clippy::let_unit_value)]
        let _ = <ConstIndex<I> as IsValidIndex<Self>>::RESULT;
        // SAFETY: We do the bounds check in IsValidIndex
        unsafe { self.get_unchecked(I) }
    }
}

pub struct ConstRange<const S: usize, const L: usize>;

impl<T, const S: usize, const L: usize, const N: usize> IsValidIndex<[T; N]> for ConstRange<S, L> {
    const RESULT: () = assert!(N >= S + L, "End index is out of bounds!");
}

impl<T, const S: usize, const L: usize, const N: usize> Index<ConstRange<S, L>> for [T; N] {
    type Output = [T; L];
    fn index(&self, _: ConstRange<S, L>) -> &[T; L] {
        #[allow(clippy::let_unit_value)]
        let _ = <ConstRange<S, L> as IsValidIndex<Self>>::RESULT;
        // SAFETY: We do the bounds check in IsValidIndex
        unsafe { &*(self.as_ptr().add(S) as *const [T; L]) }
    }
}
