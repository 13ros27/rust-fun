use crate::cond::Cond;

mod imp {
    trait IsCopyTest {
        const IS_COPY: bool;
    }

    impl<T: ?Sized> IsCopyTest for T {
        default const IS_COPY: bool = false;
    }

    impl<T: ?Sized + Copy> IsCopyTest for T {
        const IS_COPY: bool = true;
    }

    pub const fn is_copy<T: ?Sized>() -> bool {
        <T as IsCopyTest>::IS_COPY
    }
}

pub type Copyable<'a, T> = Cond<{ imp::is_copy::<T>() }, T, &'a T>;

// Waiting on proper associated_const_equality
// trait GetCopy<'a> {
//     fn get_copy(&'a self) -> Copyable<'a, Self>;
// }

// impl<'a, T: ?Sized> GetCopy<'a> for T {
//     default fn get_copy(&'a self) -> Copyable<'a, Self> {
//         self
//     }
// }

// impl<'a, T: ?Sized + Copy> GetCopy<'a> for T {
//     fn get_copy(&'a self) -> Copyable<'a, Self> {
//         *self
//     }
// }
