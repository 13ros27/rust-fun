mod imp {
    use std::marker::PhantomData;

    #[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Cond<const B: bool, T: ?Sized, F: ?Sized>(PhantomData<T>, PhantomData<F>);

    pub trait AssocType {
        type Type: ?Sized;
    }

    impl<T: ?Sized, F: ?Sized> AssocType for Cond<false, T, F> {
        type Type = F;
    }

    impl<T: ?Sized, F: ?Sized> AssocType for Cond<true, T, F> {
        type Type = T;
    }
}

pub type Cond<const B: bool, T, F> = <imp::Cond<B, T, F> as imp::AssocType>::Type;
