use std::array;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

pub trait UnzipArray<I: Iterator<Item = [T; N]>, T, const N: usize> {
    fn unzip_array(self) -> [UnzipSection<I, T, N>; N];
}

impl<I: Iterator<Item = [T; N]>, T, const N: usize> UnzipArray<I, T, N> for I {
    fn unzip_array(self) -> [UnzipSection<I, T, N>; N] {
        let buffer = Rc::new(RefCell::new(UnzipBuffer::new(self)));
        array::from_fn(|i| UnzipSection::new(buffer.clone(), i))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct UnzipBuffer<I: Iterator<Item = [T; N]>, T, const N: usize> {
    iterator: I,
    buffered: [VecDeque<T>; N],
}

impl<I: Iterator<Item = [T; N]>, T, const N: usize> UnzipBuffer<I, T, N> {
    fn new(iterator: I) -> Self {
        Self {
            iterator,
            buffered: array::from_fn(|_| VecDeque::new()),
        }
    }
}

#[derive(Clone)]
pub struct UnzipSection<I: Iterator<Item = [T; N]>, T, const N: usize> {
    buffer: Rc<RefCell<UnzipBuffer<I, T, N>>>,
    index: usize,
}

impl<I: Iterator<Item = [T; N]>, T, const N: usize> UnzipSection<I, T, N> {
    fn new(buffer: Rc<RefCell<UnzipBuffer<I, T, N>>>, index: usize) -> Self {
        Self { buffer, index }
    }
}

impl<I: Iterator<Item = [T; N]>, T, const N: usize> Iterator for UnzipSection<I, T, N> {
    type Item = T;

    fn next(&mut self) -> Option<T> {
        let mut buffer = self.buffer.borrow_mut();

        if buffer.buffered[self.index].is_empty() {
            if let Some(a) = buffer.iterator.next() {
                buffer
                    .buffered
                    .iter_mut()
                    .zip(a)
                    .for_each(|(v, i)| v.push_back(i));
            }
        }

        buffer.buffered[self.index].pop_front()
    }
}
