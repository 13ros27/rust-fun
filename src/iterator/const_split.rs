use std::array;
use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

pub trait ConstSplit: Iterator + Sized {
    fn split<const N: usize>(self) -> [SplitSection<Self, N>; N];
}

impl<I: Iterator> ConstSplit for I {
    fn split<const N: usize>(self) -> [SplitSection<I, N>; N] {
        let buffer = Rc::new(RefCell::new(SplitBuffer::new(self)));
        array::from_fn(|i| SplitSection::new(buffer.clone(), i))
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct SplitBuffer<I: Iterator, const N: usize> {
    iterator: I,
    buffered: [VecDeque<I::Item>; N],
}

impl<I: Iterator, const N: usize> SplitBuffer<I, N> {
    fn new(iterator: I) -> Self {
        Self {
            iterator,
            buffered: array::from_fn(|_| VecDeque::new()),
        }
    }
}

#[derive(Clone)]
pub struct SplitSection<I: Iterator, const N: usize> {
    buffer: Rc<RefCell<SplitBuffer<I, N>>>,
    index: usize,
}

impl<I: Iterator, const N: usize> SplitSection<I, N> {
    fn new(buffer: Rc<RefCell<SplitBuffer<I, N>>>, index: usize) -> Self {
        Self { buffer, index }
    }
}

impl<I: Iterator, const N: usize> Iterator for SplitSection<I, N> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        let mut buffer = self.buffer.borrow_mut();

        if buffer.buffered[self.index].is_empty() {
            for i in 0..buffer.buffered.len() {
                if let Some(l) = buffer.iterator.next() {
                    buffer.buffered[i].push_back(l);
                } else {
                    break;
                }
            }
        }

        buffer.buffered[self.index].pop_front()
    }
}
