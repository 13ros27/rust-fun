use super::const_iter::{ConstArrayIterMut, ConstCollect};

pub trait ZipArray {
    type Output;
    fn zip(self) -> Self::Output;
}

impl<I: IntoIterator, const N: usize> ZipArray for [I; N] {
    type Output = ZippedArray<I::IntoIter, N>;
    fn zip(self) -> ZippedArray<I::IntoIter, N> {
        ZippedArray(self.into_iter().map(|i| i.into_iter()).const_collect())
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ZippedArray<I: Iterator, const N: usize>([I; N]);

impl<I: Iterator, const N: usize> Iterator for ZippedArray<I, N> {
    type Item = [Option<I::Item>; N];

    fn next(&mut self) -> Option<Self::Item> {
        let mut contains_some = false;
        let zipped = self
            .0
            .const_iter_mut()
            .map(|i| {
                let next = i.next();
                contains_some |= next.is_some();
                next
            })
            .const_collect();
        if contains_some {
            Some(zipped)
        } else {
            None
        }
    }
}
