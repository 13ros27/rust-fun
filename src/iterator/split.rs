use std::cell::RefCell;
use std::collections::VecDeque;
use std::rc::Rc;

pub trait Split: Iterator + Sized {
    fn split(self, sections: usize) -> Vec<SplitSection<Self>>;
}

impl<I: Iterator> Split for I {
    fn split(self, sections: usize) -> Vec<SplitSection<I>> {
        let buffer = Rc::new(RefCell::new(SplitBuffer::new(self, sections)));
        (0..sections)
            .map(|i| SplitSection::new(buffer.clone(), i))
            .collect()
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct SplitBuffer<I: Iterator> {
    iterator: I,
    buffered: Box<[VecDeque<I::Item>]>,
}

impl<I: Iterator> SplitBuffer<I> {
    fn new(iterator: I, sections: usize) -> Self {
        Self {
            iterator,
            buffered: (0..sections)
                .map(|_| VecDeque::new())
                .collect::<Vec<_>>()
                .into_boxed_slice(),
        }
    }
}

#[derive(Clone)]
pub struct SplitSection<I: Iterator> {
    buffer: Rc<RefCell<SplitBuffer<I>>>,
    index: usize,
}

impl<I: Iterator> SplitSection<I> {
    fn new(buffer: Rc<RefCell<SplitBuffer<I>>>, index: usize) -> Self {
        Self { buffer, index }
    }
}

impl<I: Iterator> Iterator for SplitSection<I> {
    type Item = I::Item;

    fn next(&mut self) -> Option<I::Item> {
        let mut buffer = self.buffer.borrow_mut();

        if buffer.buffered[self.index].is_empty() {
            for i in 0..buffer.buffered.len() {
                if let Some(l) = buffer.iterator.next() {
                    buffer.buffered[i].push_back(l);
                } else {
                    break;
                }
            }
        }

        buffer.buffered[self.index].pop_front()
    }
}
